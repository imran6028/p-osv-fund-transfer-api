# p-osv-fund-transfer-api

The API shall be invoked from Experience API for various fundtransfer use-case scenarios.
The service has capability to process single and/or multiple
payments of one bank or multiple banks* of any one category
(interbank/sarie/swift) at a time.
